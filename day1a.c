#include <stdio.h>
#include <stdlib.h>


int main(void)
{
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int total=0;
  int max=0;
  fp = fopen("./input.txt", "r");
  if (fp == NULL)
    exit(1);
  while ((read = getline(&line, &len, fp)) != -1) {
    if (read==1){
      if (total>max) max=total;
      total=0;
    }else{
      int number=atoi(line);
      total+=number;
    }
    //printf("Retrieved line of length %zu :\n", read);
    //printf("%s", line);
  }
  printf("%d\n",max);
  if (ferror(fp)) {
    /* handle error */
  }
  free(line);
  fclose(fp);
  return 0;
}
