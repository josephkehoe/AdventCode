#include <stdio.h>
#include <stdlib.h>
#define MAXELVES 3

int main(void)
{
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int total=0;
  int max[MAXELVES];
  //initialise array to zero's
  for(int i=0;i<MAXELVES;++i){
    max[i]=0;
  }
  fp = fopen("./input.txt", "r");
  if (fp == NULL)
    exit(1);
  while ((read = getline(&line, &len, fp)) != -1) {
    if (read==1){//blank line
      int temp=0;
      for(int i=0;i<MAXELVES;++i){//insert total into sorted max array
        if (total>max[i]){//swap total and array entry
          temp=max[i];
          max[i]=total;
          total=temp;
        }
      }
      total=0;//reset total for next elf
    }else{//add to total
      int number=atoi(line);
      total+=number;
    }
    //printf("Retrieved line of length %zu :\n", read);
    //printf("%s", line);
  }
  total=0;
  for(int i=0;i<MAXELVES;++i){
    total += max[i];
  }
  printf("%d\n",total);
  if (ferror(fp)) {
    /* handle error */
  }
  free(line);
  fclose(fp);
  return 0;
}
