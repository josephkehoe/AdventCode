#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define FALSE 0
#define TRUE 1
#define BUFFERLEN 4

typedef unsigned short Bool;

Bool noReps(char* theBuffer,int bufSize){
  Bool found=FALSE;
  for(int i=0;i<bufSize-1;++i){
    for(int k=i+1;k<bufSize;++k){
      if(theBuffer[i]==theBuffer[k]){
	found= TRUE;
      }//if rep found
    }//inner for buffer iterate
  }//outer for buffer iterate
  if (found==TRUE) return FALSE; else return TRUE;
}//noReps

int main(void)
{
  FILE *fp;
  char *line=NULL;
  size_t len = 0;
  ssize_t read;
  int lineCount=0;
  Bool setup=TRUE;
  char amount[3];
  char from;
  char to;
  int charCount=0;
  Bool allDifferent=FALSE;
  char Buffer[BUFFERLEN];

  
  fp = fopen("./day6Input.txt", "r");
  if (fp == NULL)
    exit(1);
  if((read = getline(&line, &len, fp)) != -1) {
      int size=strlen(line);
      Buffer[0]=line[0];
      Buffer[1]=line[1];
      Buffer[2]=line[2];
      int bufCount=3;//start at 4th character
      for(charCount=3;charCount<size && allDifferent==FALSE;++charCount){
	Buffer[bufCount]=line[charCount];
	bufCount=(bufCount+1)%BUFFERLEN;
	allDifferent=noReps(Buffer,BUFFERLEN);
	printf("%c",line[charCount]);
      }//for chars in line
      printf("\nfound at: %d 0f %d\n",charCount,size);
      printf("BUFFER:%s\n",Buffer);
  }//if file read

  printf("\n\n");

  if (ferror(fp)) {
    /* handle error */
  }
  free(line);
  fclose(fp);
  return 0;
}
