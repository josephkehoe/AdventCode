package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	//Open File
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(file)
	var firstList, secondList []int
	var first, second int
	var diff, simIndex int
	//Read in file line at a time
	scanner := bufio.NewScanner(file)
	for scanner.Scan() { //read two integers from each line
		curLine := scanner.Text()
		stringArray := strings.Split(curLine, "   ")
		//fmt.Println(stringArray[0], "+", stringArray[1])
		first, err = strconv.Atoi(stringArray[0])
		if err != nil {
			fmt.Println(err)
		}
		second, err = strconv.Atoi(stringArray[1])
		if err != nil {
			fmt.Println(err)
		}
		firstList = append(firstList, first)
		secondList = append(secondList, second)
	}
	//Check for any file read errors
	if err := scanner.Err(); err != nil { //file read error
		fmt.Println(err)
	} //file read error

	//PART TWO SOLUTION
	simIndex = 0
	for _, value := range firstList {
		simIndex = simIndex + value*occurances(value, secondList)
	}
	fmt.Println("PART TWO:", simIndex)
	//PART ONE SOLUTION
	sort.Ints(firstList)
	sort.Ints(secondList)
	//fmt.Println(firstList)
	//fmt.Println(secondList)
	for i, value := range firstList {
		if value > secondList[i] {
			diff = diff + value - secondList[i]
		} else {
			diff = diff + secondList[i] - value
		}
	}
	fmt.Println("PART ONE: ", diff)
}

func occurances(value int, list []int) int {
	var answer int
	answer = 0
	for _, theNumber := range list {
		if theNumber == value {
			answer = answer + 1
		}
	}
	return answer
} //main
