package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	//Open File
	file, err := os.Open("output1.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(file)
	var value int
	var total int
	total = 0
	//Read in file line at a time
	scanner := bufio.NewScanner(file)
	for scanner.Scan() { //read two integers from each line
		var theList []int
		curLine := scanner.Text()
		stringArray := strings.Split(curLine, " ")
		for _, s := range stringArray { //convert to ints
			value, err = strconv.Atoi(s)
			if err != nil {
				fmt.Println(err)
			}
			theList = append(theList, value)
		} //convert to ints
		if checkSafety2(theList) == true {
			total++
		} else if (sorted(theList)) == true && !diffed(theList) == true {
			fmt.Println(theList)
		}
	}
	fmt.Println(total)
	//Check for any file read errors
	if err := scanner.Err(); err != nil { //file read error
		fmt.Println(err)
	} //file read error

}
func checkSafety(theList []int) bool {
	inc := true
	dec := true
	safe := true
	for index := 0; index < len(theList)-1; index++ {
		if theList[index] > theList[index+1] {
			inc = false
		}
		if theList[index] < theList[index+1] {
			dec = false
		}
		if theList[index] == theList[index+1] {
			safe = false
		}
		diff := theList[index] - theList[index+1]
		if diff > 3 || diff < -3 {
			safe = false
		}
	}
	if inc && safe {
		return true
	} else if dec && safe {
		return true
	} else {
		return false
	}
}

func sorted(theList []int) bool {
	inc := true
	dec := true
	for i := 0; i < len(theList)-1; i++ {
		if theList[i] < theList[i+1] {
			inc = false
		} else if theList[i] > theList[i+1] {
			dec = false
		}
	}
	return dec || inc
}

func diffed(theList []int) bool {
	ok := true

	for i := 0; i < len(theList)-1; i++ {
		diff := theList[i] - theList[i+1]
		if diff > 3 || diff < -3 {
			ok = false
		}
	}
	return ok
}

func checkSafety2(theList []int) bool {
	inc := true
	dec := true
	safe := true
	allowable := true

	for index := 0; index < len(theList)-1; index++ {
		if theList[index] > theList[index+1] { //INCREASING PROPERTY
			if allowable == false {
				inc = false
			} else if index+2 < len(theList)-1 && theList[index] < theList[index+2] {
				allowable = false
				index++
			} else if index+1 == len(theList)-1 { //end of list all ok
				allowable = false
			} else {
				inc = false
			}
		} //INCREASING PROPERTY
		if theList[index] < theList[index+1] {
			if allowable == false {
				dec = false
			} else if index+2 < len(theList)-1 && theList[index] > theList[index+2] {
				allowable = false
				index++
			} else if index+1 == len(theList)-1 {
				allowable = false
			} else {
				dec = false
			}
		}
		//PAIR FOUND
		if theList[index] == theList[index+1] {
			if allowable {
				//index++
				allowable = false
			} else {
				safe = false
			}
		}
		diff := theList[index] - theList[index+1]
		if diff < 0 {
			diff = -diff
		}
		if diff > 3 {
			if allowable == false { //second match --stop
				safe = false
			} else if index == len(theList)-2 { //end of array --ignore
				allowable = false
			} else { //first match
				allowable = false
				solutionFound := false
				if index > 0 {
					prevDiff := theList[index-1] - theList[index+1]
					if prevDiff < 0 {
						prevDiff = -prevDiff
					}
					if prevDiff < 3 {
						solutionFound = true
					}
				}
				if solutionFound == false && index < len(theList)-2 {
					nextDiff := theList[index] - theList[index+2]
					if nextDiff < 0 {
						nextDiff = -nextDiff
					}
					if nextDiff < 3 {
						solutionFound = true
						index++ //skip next number==it is removed
					}
				}
				if solutionFound == false {
					safe = false
				}
			}

		}
	}
	if inc && safe {
		return true
	} else if dec && safe {
		return true
	} else {
		return false
	}
}
