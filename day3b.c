#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define FALSE 0
#define TRUE 1


int convertChar(char ch){
  int value=0;
   if(ch<='Z'&& ch>='A'){//uppercase
      value=ch-'A'+27;
    }
    else{//lowercase
      value=ch-'a'+1;
    }//if-else
   return value;
}//convertChar

char findBadge(char* one, char* two, char* three){
  one[strlen(one)]=NULL;
  two[strlen(two)]=NULL;
  three[strlen(three)]=NULL;
  char *badge;
  for(int i=0;i<strlen(one);++i){
    char badge=one[i];
    for(int k=0;k<strlen(two);++k){
      if (badge==two[k]){
	for(int m=0;m<strlen(three);++m){
	  if (badge==three[m]){
	    return badge;
	  }//if found!
	}//for m
      }//if candidate found
    }//for k
  }//for i
  return NULL;
}

int main(void)
{
  FILE *fp;
  char *line[3] = {NULL,NULL,NULL};
  size_t len = 0;
  ssize_t read;
  int total=0;
  int count=0;
  char sharedChar;
  int value=0;

  int found=FALSE;
  int grpIndex=0;

  for(int i=0;i<100;++i) val[i]='@';
  fp = fopen("./backpack.txt", "r");
  if (fp == NULL)
    exit(1);
  while ((read = getline(&line[grpIndex], &len, fp)) != -1) {
    if(grpIndex==2){
      sharedChar=findBadge(line[0],line[1],line[2]);
      //spec says there is always one shared item, always an alphabetic char as well
      value=convertChar(sharedChar);
      count++;
      total+=value;
      grpIndex=0;
    }else{
      grpIndex++;
    }//if-else
  }//while 
  printf("\nTotal is %d\nLines read is %d\n",total,count);

  if (ferror(fp)) {
    /* handle error */
  }
  free(line[0]);
  free(line[1]);
  free(line[2]);
  fclose(fp);
  return 0;
}
