#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define FALSE 0
#define TRUE 1



int split(char *in, char* first, char* second, char  token){
    int returnVal=-1;
    int i=0;
    while(in[i]!=token){
        first[i]=in[i];
        ++i;
    }//while
    returnVal=i;
    first[i++]='\0';
    int k=0;
    while(i<strlen(in)){
        second[k++]=in[i++];
    }//while
    second[k]='\0';
    return returnVal;
}//split



int main(void)
{
  FILE *fp;
  char *line=NULL;
  size_t len = 0;
  ssize_t read;
  int total=0;
  int lineCount=0;
  char firstElf[10],secondElf[10];
  char lowerA[10],upperA[10],lowerB[10],upperB[10];
  int lowerValA,lowerValB,upperValA,upperValB;
  fp = fopen("./day4Input.txt", "r");
  if (fp == NULL)
    exit(1);
  while ((read = getline(&line, &len, fp)) != -1) {
      lineCount++;
      split(line,firstElf,secondElf,',');
      split(firstElf,lowerA,upperA,'-');
      split(secondElf,lowerB,upperB,'-');
      lowerValA=atoi(lowerA);
      lowerValB=atoi(lowerB);
      upperValA=atoi(upperA);
      upperValB=atoi(upperB);
      printf("%d to %d and %d to %d\n",lowerValA,upperValA,lowerValB,upperValB);
      if((lowerValA>=lowerValB)&&(upperValA<=upperValB) || ((lowerValB>=lowerValA)&&(upperValB<=upperValA))) total++;
  }//while
  printf("\nTotal is %d\nLines read is %d\n",total,lineCount);

  if (ferror(fp)) {
    /* handle error */
  }
  free(line);


  fclose(fp);
  return 0;
}
