#include <stdio.h>
#include <stdlib.h>

#define ROCK 1
#define PAPER 2
#define SCISSORS 3
#define WIN 6
#define DRAW 3
#define LOSE 0

int main(void)
{
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int total=0;
  char play, reply;;

  fp = fopen("./rockPaperScissors.txt", "r");
  if (fp == NULL)
    exit(1);
  while ((read = getline(&line, &len, fp)) != -1) {
    play=line[0];
    reply=line[2];
    if(play=='A'){//rock
      if(reply=='X'){//lose
        total=total+SCISSORS+LOSE;
      }
      else if(reply=='Y'){//draw
        total=total+ROCK+DRAW;
      }
      else if(reply=='Z'){//win
        total=total+PAPER+WIN;
      }
      else{
        printf("ERROR IN REPLY:%c",reply);
      }
    }else if (play=='B'){//paper
      if(reply=='X'){//lose
        total=total+ROCK+LOSE;
      }
      else if(reply=='Y'){//draw
        total=total+PAPER+DRAW;
      }
      else if(reply=='Z'){//win
        total=total+SCISSORS+WIN;
      }
      else{
        printf("ERROR IN REPLY:%c",reply);
      } 
    }
    else if (play=='C'){//scissors
      if(reply=='X'){//lose
        total=total+PAPER+LOSE;
      }
      else if(reply=='Y'){//draw
        total=total+SCISSORS+DRAW;
      }
      else if(reply=='Z'){//win
        total=total+ROCK+WIN;
      }
      else{
        printf("ERROR IN REPLY:%c",reply);
      }
    }
    else{//error!
      printf("ERROR IN PLAYER INPUT:%c\n",play);
    }
    //printf("Retrieved line of length %zu :\n", read);
    //printf("%s", line);
  }
  printf("%d\n",total);
  if (ferror(fp)) {
    /* handle error */
  }
  free(line);
  fclose(fp);
  return 0;
}
