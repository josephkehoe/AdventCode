#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define FALSE 0
#define TRUE 1


int convertChar(char ch){
  int value=0;
   if(ch<='Z'&& ch>='A'){//uppercase
      value=ch-'A'+27;
    }
    else{//lowercase
      value=ch-'a'+1;
    }//if-else
   return value;
}//convertChar


int main(void)
{
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int total=0;
  int count=0;
  char sharedChar;
  int value=0;
  int val[300];
  int found=FALSE;
  int types[52];
  int typeTotal=0;
  for(int i=0;i<52;++i) types[i]=0;
  fp = fopen("./backpack.txt", "r");
  if (fp == NULL)
    exit(1);
  while ((read = getline(&line, &len, fp)) != -1) {
    int size=strlen(line);
    int boundary=(int)(size/2);
    found=FALSE;
    for(int i=0;i<boundary&&found==FALSE;++i){
      for(size_t k=boundary;k<size-1&&found==FALSE;++k){
	if (line[i]==line[k]){
	  sharedChar=line[i];
	  found=TRUE;
	}//if
      }//inner for
    }//outer for
    //spec says there is always one shared item, always an alphabetic char as well
    value=convertChar(sharedChar);
    count++;
    total+=value;
    //printf("%c:%d:%d\n",sharedChar,boundary,size);
  }//while 
  printf("\nTotal is %d\nLines read is %d\n",total,count);


  if (ferror(fp)) {
    /* handle error */
  }
  free(line);
  fclose(fp);
  return 0;
}
