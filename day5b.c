#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define FALSE 0
#define TRUE 1
#define STACKPHASE 0
#define MOVEPHASE 1
#define MAXROW 9
#define MAXCOL 70
#define LINELEN 35
#define EMPTY ' '
typedef unsigned short Bool;

//data structures
//GLOBAL just to keep things simple
char stacks[MAXROW][MAXCOL];
int top[MAXROW];



int move(int amount, int from, int to){
  if (amount==0) return -1;
  //fix offsets - data structure starts at 0 not 1!!
  from--;
  to--;
  int fromStart=top[from]-amount;
  for(int i=fromStart;i<top[from];++i){
    stacks[to][top[to]]=stacks[from][i];
    stacks[from][i]=EMPTY;
    top[to]++;
  }
  top[from]=fromStart;
}//move

int insert(char box, int column){
  for(int i=top[column];i>0;--i){
    stacks[column][i]=stacks[column][i-1];
  }//for --make space for box at bottom of column
  //place nex box at bottom and increase size of stack
  stacks[column][0]=box;
  top[column]++;
}//insert

void outputStacks(void){
  for(int i=0;i<MAXROW;++i){
    for(int k=0;k<20;++k){
      printf("[%c]",stacks[i][k]);
    }
    printf("\n");
  }
  for(int i=0;i<MAXROW;++i) printf("{%d}",top[i]);
  printf("=============================\n");
}//outputStacks



int main(void)
{
  FILE *fp;
  char *line=NULL;
  size_t len = 0;
  ssize_t read;
  int lineCount=0;
  Bool setup=TRUE;
  char amount[3];
  char from;
  char to;
  int offset=0;
  Bool out=TRUE;
  //init data structures
  for(int i=0;i<MAXROW;++i){
    top[i]=0;
    for(int k=0;k<MAXCOL;++k){
      stacks[i][k]=EMPTY;
    }//for k
  }//for i

  
  fp = fopen("./day5Input.txt", "r");
  if (fp == NULL)
    exit(1);
  while ((read = getline(&line, &len, fp)) != -1) {
      lineCount++;
      int size=strlen(line);
      if (size<10){
	//skip -- empty line
	setup=FALSE;//move phase begins next
	outputStacks();
      }else if(setup==TRUE){
	for(int i=1,k=0;i<LINELEN;i+=4,++k){
	  if (isalpha(line[i])) insert(line[i],k);
	}//for row of boxes
      }else{//move phase
	amount[0]=line[5];
	if (isdigit(line[6])){
	    amount[1]=line[6];
	    amount[2]='\0';
	    offset=1;
	  }else{
	    amount[1]='\0';
	    offset=0;
	  }
	  from=line[12+offset];
	  to=line[17+offset];
	  // 
	  if(!isdigit(from) || !isdigit(to)){
	    printf("offset is wrong line:%d from:%c to:%c \n",lineCount,from,to);
	    exit(1);
	  }//sanity error check on reading in numbers
	  move(atoi(amount),(int)from-'0',(int)to-'0');
	  if(out==TRUE){
	    printf("\namt:%d  from:%d  to:%d\n",atoi(amount),(int)from-'0',(int)to-'0');
	    outputStacks();
	    char ch=getc(stdin);
	    if(ch=='q') out=FALSE;
	  }//if print table
      }//if-else

  }//end of file read

  //ouput top of stack columns
  for(int i=0;i<MAXROW;++i){
    printf("%c",stacks[i][top[i]-1]);
  }
  printf("\n\n");
  outputStacks();
  if (ferror(fp)) {
    /* handle error */
  }
  free(line);


  fclose(fp);
  return 0;
}
